package ejemplos;

import java.util.Scanner;

public class Distancia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner e;
		float num1, metro = 1609;
		float resultado;
		
		e = new Scanner(System.in);
		
		System.out.println("Ingrese la cantidad de millas:");
		num1 = e.nextFloat();

		resultado = num1 * metro;
		
		System.out.println(num1+" millas equivale a "+resultado+" metros");

	}

}
