package ejemplos;

import java.util.Scanner;

public class Multiplicacion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner e;
		int num1, num2;
		int producto;
		
		e = new Scanner(System.in);
		
		System.out.println("Ingrese el primer número:");
		num1 = e.nextInt();
		System.out.println("Ingrese el segundo número:");
		num2 = e.nextInt();
		
		producto = num1 * num2;
		
		System.out.println("La producto entre "+num1+" y "+num2+" es: "+producto);

	}

}
