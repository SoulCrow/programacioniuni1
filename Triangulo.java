package ejemplos;

import java.util.Scanner;

public class Triangulo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner e;
		int base, altura;
		float superficie;
		
		e = new Scanner(System.in);
		
		System.out.println("Ingrese la base del triángulo:");
		base = e.nextInt();
		System.out.println("Ingrese la altura del triángulo:");
		altura = e.nextInt();
		
		superficie = (base * altura) / 2;
		
		System.out.println("La superficie del triángulo es: "+superficie);

	}

}
