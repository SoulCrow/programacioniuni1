package ejemplos;

import java.util.Scanner;

public class Temperatura {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner e;
		float centigrados;
		float fahrenheit;
		
		e = new Scanner(System.in);
		
		System.out.println("Ingrese la temperatura en ºC:");
		centigrados = e.nextFloat();

		fahrenheit = 9 * (centigrados/5) + 32;
		
		System.out.println(centigrados+" ºC equivale a "+fahrenheit+" ºF");

	}

}
