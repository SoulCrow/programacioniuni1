package ejemplos;

import java.util.Scanner;

public class Division {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner e;
		float num1, num2;
		float div;
		
		e = new Scanner(System.in);
		
		System.out.println("Ingrese el primer número:");
		num1 = e.nextInt();
		System.out.println("Ingrese el segundo número:");
		num2 = e.nextFloat();
		
		div = num1/num2;
		
		System.out.println("La división entre "+num1+" y "+num2+" es: "+div);

	}

}
